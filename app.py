# encoding: utf-8
# Criado por Caio Rondon - Lab. APR - 2017.1
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.wsgi

from handlers.config_handler import ConfigHandler
from handlers.cadastro_handler import CadastroHandler
from handlers.consulta_handler import ConsultaHandler

# Momoko para conexao com postgresql usando psycopg2
import momoko
from psycopg2.extras import RealDictCursor

# Motor para conexao com mongodb
import motor.motor_tornado

# sys para obter argumentos da linha de comando
import sys

def create_web_server():
    # Roteamento para as diferentes URIs
    handlers = [
        (r"/consulta", ConsultaHandler),
        (r"/cadastro", CadastroHandler),
    ]

    # Configurações da aplicação
    settings = dict(
        #template_path='templates/'
    )

    return tornado.web.Application(handlers, **settings)


if __name__ == '__main__':
    # Le a porta a ser usada a partir da configuracao lida
    # http_listen_port = ConfigHandler.http_listen_port
    print("Iniciando servico")
    http_listen_port = sys.argv[1]
    web_app = create_web_server()
    ioloop = tornado.ioloop.IOLoop.instance()

    # Pool do momoko
    conn_dsn = 'dbname=' + ConfigHandler.postgresql_db_name + ' user=' + ConfigHandler.postgresql_user \
               + ' password=' + ConfigHandler.postgresql_password + ' host=' + ConfigHandler.postgresql_address \
               + ' port=' + str(ConfigHandler.postgresql_port)

    print("conexao com o banco")
    web_app.db = momoko.Pool(
        dsn=conn_dsn,
        size=ConfigHandler.postgresql_conn_pool_size,
        ioloop=ioloop,
        cursor_factory=RealDictCursor
    )

    try:
        # Conexao com o banco de dados usando Momoko
        future = web_app.db.connect()
        ioloop.add_future(future, lambda f: ioloop.stop())
        ioloop.start()
        future.result()  # raises exception on connection error
    except:
        print('err connecting to postgresql')

    # Inicia servidor web
    web_app.listen(http_listen_port)
    print("started")
    ioloop.start()
