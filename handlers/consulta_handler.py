import tornado.web
import simplejson

from tornado import gen

import hashlib
import simplejson

class ConsultaHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def check_user(self, login, senha):
        query_sql = """ SELECT * FROM usuario WHERE login = %(login)s AND senha = %(senha)s """

        values = {
            "login": login,
            "senha": hashlib.sha512(senha.encode('utf-8')).hexdigest()
        }

        cursor = yield self.application.db.execute(query_sql, values)
        result = cursor.fetchone()

        if result:
            return True
        else:
            return False


    @gen.coroutine
    def post(self):
        # preenche campos
        login = self.get_argument("login", default=None)
        senha = self.get_argument("senha", default=None)
        cpf_cnpj = self.get_argument("cpf_cnpj", default=None)
        renavam = self.get_argument("renavam", default=None)

        # checa se os campos login e senha (obrigatorios) estao presentes
        if login is None or login == "" or senha is None or senha == "":
            response = {
                "status": "error",
                "message": "invalid or missing field"
            }

            self.set_status(400)
            self.write(response)
            return

        # checa se o usuario possui conta na plataforma
        if (yield self.check_user(login, senha)):
            ## se um dos dois nao foi fornecido, retorna todos
            if cpf_cnpj is None or cpf_cnpj == "" or renavam is None or renavam == "":
                query_sql = """ SELECT * FROM registro"""
                cursor = yield self.application.db.execute(query_sql)
                results = cursor.fetchall()

                self.set_status(200)
                self.write(simplejson.dumps(results))
                return

            else:
                query_sql = """ SELECT * FROM registro WHERE cpf_cnpj = %(cpf_cnpj)s AND renavam = %(renavam)s"""

                values = {
                    "cpf_cnpj": cpf_cnpj,
                    "renavam": renavam
                }

                cursor = yield self.application.db.execute(query_sql, values)
                result = cursor.fetchone()

                if result:
                    self.set_status(200)
                    self.write(result)
                    return

                else:
                    response = {
                        "status": "error",
                        "message": "nothing found"
                    }

                    self.set_status(404)
                    self.write(response)
                    return

        # se usuario nao possuir, erro.
        else:
            response = {
                "status": "error",
                "message": "invalid user or password"
            }

            self.set_status(401)
            self.write(response)
            return

        return
