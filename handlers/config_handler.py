import yaml
import sys
import os


class ConfigHandler:
    # Le arquivo de configuracao para obter em que porta HTTP escutar
    config_file_stream = open(os.path.join(sys.path[0], "config.yaml"), "r");
    config = yaml.load(config_file_stream)

    postgresql_conn_pool_size = config["database"]["postgresql"]["pool_size"]
    postgresql_user = config["database"]["postgresql"]["user"]
    postgresql_password = config["database"]["postgresql"]["password"]
    postgresql_db_name = config["database"]["postgresql"]["db_name"]
    postgresql_address = config["database"]["postgresql"]["address"]
    postgresql_port = config["database"]["postgresql"]["port"]

    config_file_stream.close()
