import tornado.web
import simplejson

from tornado import gen

import hashlib
import simplejson

class CadastroHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def post(self):
        login = self.get_argument("login", default=None)
        senha = self.get_argument("senha", default=None)

        if login is None or login == "" or senha is None or senha == "":
            response = {
                "status": "error",
                "message": "invalid or missing fields"
            }

            self.set_status(400)
            self.write(response)
            return

        ## checa se usuario ja existe
        query_sql = """ SELECT * FROM usuario WHERE login = %(login)s"""

        values = {
            "login": login
        }

        cursor = yield self.application.db.execute(query_sql, values)
        result = cursor.fetchone()

        # ja existe
        if result:
            response = {
                "status": "error",
                "message": "user already exists"
            }

            self.set_status(400)
            self.write(response)
            return

        # se nao existe, cria`
        else:

            query_sql = """ INSERT INTO usuario (login, senha) VALUES (%(login)s,%(senha)s)"""

            # encoda senha
            values = {
                "login": login,
                "senha": hashlib.sha512(senha.encode('utf-8')).hexdigest()
            }

            try:
                cursor = yield self.application.db.execute(query_sql, values)
                response = {
                    "status": "ok",
                    "message": "user created"
                }

                self.set_status(200)
                self.write(response)
                return

            except Exception as e:
                print(e)
                response = {
                    "status": "error",
                    "message": "error inserting in db"
                }

                self.set_status(400)
                self.write(response)
                return
